export interface UserResponseSchema {
    firstName: string;
    lastName: string;
    username: string;
    id: number;
    isBot: boolean;
    isPremium: boolean;
    languageCode: string;
    bot: string;
}

export type UserSchema = Omit<UserResponseSchema, 'error'>;

export interface BalanceResponseSchema {
    earnPassivePerSec: number;
    earnPassivePerHour: number;
    earnPassivePerDay: number;
    earnPassivePerWeek: number;
    balanceCoins: number;
    availableTaps: number;
    lastSyncUpdate: number;
    exchangeId: number;
    lastPassiveEarn: number;
    level: number;
    rankEstimated: string;
}

export type BalanceSchema = Omit<BalanceResponseSchema, 'error'>;

interface LevelSchema {
    level: number;
    coinsToLeveUp: number | null;
}

export interface ConfigResponseSchema {
    userLevels_balanceCoins: LevelSchema[];
}

export interface Level {
    name: string;
    coins: number | null;
}

export type ConfigSchema = Omit<ConfigResponseSchema, 'error'>;

export interface CardConditionSchema {
    _type: string;
    upgradeId: string;
    level: number;
    referralCount: number;
    moreReferralsCount: number;
}

export interface CardsResponseSchema {
    id: string;
    name: string;
    section: string;
    currentProfitPerHour: number;
    profitPerHour: number;
    profitPerHourDelta: number;
    price: number;
    level: number;
    isAvailable: boolean;
    isExpired: boolean;
    condition: CardConditionSchema;
    efficiency: number;
    cooldownSeconds: number;
}

export type CardsSchema = Omit<CardsResponseSchema, 'error'>;
