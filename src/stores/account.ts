import {atom} from 'nanostores';
import {BalanceResponseSchema, CardsResponseSchema, Level, UserResponseSchema} from "./schemas.ts";

const $tokenStore = atom<string>("");

const setToken = (token: string) => {
    $tokenStore.set(token);
}

const clearToken = () => {
    $tokenStore.set("");
}

const getToken = (): string | null => {
    const token = $tokenStore.get();
    if (token) {
        return token
    }

    return null;
}

export const TokenStore = {
    $tokenStore,
    setToken,
    clearToken,
    getToken,
}

const $balanceStore = atom<BalanceResponseSchema | null>(null);

const setBalance = (balance: BalanceResponseSchema) => {
    $balanceStore.set(balance);
}

const clearBalance = () => {
    $balanceStore.set(null);
}

const getBalance = (): BalanceResponseSchema | null => {
    return $balanceStore.get();
}

export const BalanceStore = {
    $balanceStore,
    setBalance,
    getBalance,
    clearBalance,
}

const $configStore = atom<Map<number, Level>>(new Map<number, Level>);

const setConfig = (config: Map<number, Level>) => {
    $configStore.set(config);
}

const clearConfig = () => {
    $configStore.set(new Map<number, Level>);
}

const getConfig = (): Map<number, Level> => {
    return $configStore.get();
}

export const ConfigStore = {
    $configStore,
    setConfig,
    clearConfig,
    getConfig,
}

const $userStore = atom<UserResponseSchema | null>(null);

const setUserStore = (user: UserResponseSchema) => {
    $userStore.set(user);
}

const clearUserStore = () => {
    $userStore.set(null);
}

const getUserStore = (): UserResponseSchema | null => {
    return $userStore.get();
}

export const UserStore = {
    $userStore,
    setUserStore,
    clearUserStore,
    getUserStore,
}

const $cardsStore = atom<CardsResponseSchema[]>([]);

const setCardsStore = (cards: CardsResponseSchema[]) => {
    $cardsStore.set(cards);
}

const clearCardsStore = () => {
    $cardsStore.set([]);
}

const getCardsStore = (): CardsResponseSchema[] => {
    return $cardsStore.get();
}

export const CardsStore = {
    $cardsStore,
    setCardsStore,
    clearCardsStore,
    getCardsStore,
}

const $filteredCards = atom<CardsResponseSchema[]>([]);

const setFilteredCardsStore = (filteredCards: CardsResponseSchema[]) => {
    $filteredCards.set(filteredCards);
}

const clearFilteredCardsStore = () => {
    $filteredCards.set([]);
}

const getFilteredCardsStore = (): CardsResponseSchema[] => {
    return $filteredCards.get();
}

export const FilteredStore = {
    $filteredCards,
    setFilteredCardsStore,
    clearFilteredCardsStore,
    getFilteredCardsStore,
}