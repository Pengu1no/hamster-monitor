export const addSpaces = (n: number) => {
    let res = ''
    const src = Math.floor(n).toString();

    for (let pos = src.length-1; pos >= 0; pos -= 3) {
        if (pos < 3) {
            res = `${src.substring(0, pos + 1)} ${res}`;
        } else {
            res = `${src.substring(pos-2, pos + 1)} ${res}`;
        }
    }

    return res;
}