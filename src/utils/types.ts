import {
    BalanceResponseSchema,
    CardsResponseSchema,
    ConfigResponseSchema,
    UserResponseSchema
} from "../stores/schemas.ts";

export interface BalanceResponse {
    clickerUser: BalanceResponseSchema
}

export interface CardsResponse {
    upgradesForBuy: CardsResponseSchema[];
}

export interface UserResponse {
    telegramUser: UserResponseSchema;
}

export interface ConfigResponse {
    config: ConfigResponseSchema;
}

export interface Level {
    name: string;
    coins: number | null;
}
