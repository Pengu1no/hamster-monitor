import {BalanceStore, CardsStore, ConfigStore, TokenStore, UserStore} from "../stores/account.ts";
import {API} from "../api/base.ts";
import {AxiosResponse} from "axios";
import {BalanceResponse, CardsResponse, ConfigResponse, Level, UserResponse} from "./types.ts";

const getBalance = async (token: string | null = null) => {
    if (!token) {
        token = TokenStore.getToken();
    }

    const res = await API.post<BalanceResponse, AxiosResponse<BalanceResponse>>('clicker/sync',
        {},
        {headers: {Authorization: `Bearer ${token}`}}
    )
    const {...balance} = res.data.clickerUser;

    balance.earnPassivePerDay = balance.earnPassivePerHour * 24;
    balance.earnPassivePerWeek = balance.earnPassivePerDay * 7;

    // const millisEstimated = ((1_000_000_000 - balance.balanceCoins) / balance.earnPassivePerSec) * 1000
    // const now = new Date();
    //
    // balance.rankEstimated = new Date(now.getTime()+millisEstimated).toLocaleString()

    BalanceStore.setBalance(balance);
}

const getCards = async (token: string | null = null) => {
    if (!token) {
        token = TokenStore.getToken();
    }

    const res = await API.post<CardsResponse, AxiosResponse<CardsResponse>>('clicker/upgrades-for-buy',
        {},
        {headers: {Authorization: `Bearer ${token}`}}
    )
    const cards = res.data.upgradesForBuy;

    CardsStore.setCardsStore(cards);

    return cards;
}

const getUserInfo = async (token: string | null = null) => {
    if (!token) {
        token = TokenStore.getToken();
    }

    const res = await API.post<UserResponse, AxiosResponse<UserResponse>>('auth/me-telegram',
        {},
        {headers: {Authorization: `Bearer ${token}`}}
    )

    const {...user} = res.data.telegramUser;

    user.bot = user.isBot ? "YES" : "NO";

    UserStore.setUserStore(user);
}

const getConfig = async (token: string | null = null) => {
    if (!token) {
        token = TokenStore.getToken();
    }

    const res = await API.get<ConfigResponse, AxiosResponse<ConfigResponse>>('clicker/config/bx2ng1XWjoOJeqIU5Vcfg0q73qATqA2mDr4M3tbun30',
        {headers: {Authorization: `Bearer ${token}`}}
    )
    const {...config} = res.data.config;


    let levels = new Map<number, Level>();

    config.userLevels_balanceCoins.forEach((level, index) => {
        let name: string;
        switch (level.level) {
            case 1:
                name = "Bronze"
                break;
            case 2:
                name = "Silver"
                break;
            case 3:
                name = "Gold"
                break;
            case 4:
                name = "Platinum"
                break;
            case 5:
                name = "Diamond"
                break;
            case 6:
                name = "Epic"
                break;
            case 7:
                name = "Legendary"
                break;
            case 8:
                name = "Master"
                break;
            case 9:
                name = "Grandmaster"
                break;
            case 10:
                name = "Lord"
                break;
            case 11:
                name = "Creator"
                break;
            default:
                name = level.level.toString();
                break;
        }
        levels.set(level.level, {name: name, coins: index > 0 ? config.userLevels_balanceCoins[index-1].coinsToLeveUp : 0})
    })

    ConfigStore.setConfig(levels);
}

const fetchData = async (token: string) => {
    await getUserInfo(token).then(() => getConfig(token)).then(() => getCards(token)).then(() => getBalance(token));
    // await getUserInfo(token);
    // await getConfig(token);
    // await getCards(token);
    // await getBalance(token);
}

export const Net = {
    getBalance,
    getCards,
    getUserInfo,
    getConfig,
    fetchData,
}