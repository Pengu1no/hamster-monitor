import './App.css'
import Balance from "./components/Balance/Balance.tsx";
import NavBar from "./components/Navigation/Navigation.tsx";
import Cards from "./components/Cards/Cards.tsx";
import Cipher from "./components/Cipher/Cipher.tsx";
import User from "./components/User/User.tsx";
import {Route, Routes} from "react-router-dom";
import Bearer from "./components/Bearer/Bearer.tsx";

import hamsterLogo from '/hamster-coin.png'
import {TokenStore} from "./stores/account.ts";
import {useEffect, useRef, useState} from "react";
import {Net} from "./utils/net.ts";

function App() {
    const intervalRef = useRef<ReturnType<typeof setInterval>>();

    useEffect(() => {
        // Set up interval to call getBalance every 2 hours
        intervalRef.current = setInterval(async () => {
            const token = TokenStore.getToken();

            if (!!token) {
                await Net.fetchData(token);
            }
        }, 2 * 60 * 60 * 1000); // 2 hours in milliseconds

        return () => {
            if (intervalRef.current) {
                clearInterval(intervalRef.current);
            }
        };
    }, []);

    const [target, setTarget] = useState<number>(0);

    return (
        <>
            <img className="logo" src={hamsterLogo} alt=""/>
            <h1>Hamster Kombat</h1>

            <Bearer fetchData={Net.fetchData}/>

            <Balance setTarget={setTarget}/>

            <NavBar/>

            <Routes>
                <Route path="/" element={<Cards target={target} getBalance={Net.getBalance} getCards={Net.getCards}/>}/>
                <Route path="/cipher" element={<Cipher/>}/>
                <Route path="/user" element={<User/>}/>
            </Routes>
        </>
    )
}

export default App
