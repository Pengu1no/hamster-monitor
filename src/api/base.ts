import axios from "axios";

const API = axios.create({
    baseURL: "https://api.hamsterkombatgame.io/",
    timeout: 5000,
    headers: {
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7",
        // "Connection": "keep-alive",
    }
})

export {API};