import {FieldValues, useForm} from "react-hook-form";
import {TokenStore} from "../../stores/account.ts";
import {Input} from "../../ui/Input/Input.tsx";
import {createSearchParams, useSearchParams} from "react-router-dom";
import {FC} from "react";

interface BearerProps {
    fetchData: (token: string) => {};
}

const Bearer: FC<BearerProps> = ({fetchData}) => {
    const [query, setQuery] = useSearchParams();

    const {
        handleSubmit,
        control,
    } = useForm<FieldValues>({
        values: {
            token: query.get('token'),
        }
    })

    const onSubmit = (data: FieldValues) => {
        const {
            token
        } = data;

        TokenStore.setToken(token);

        TokenStore.$tokenStore.get();
        setQuery(createSearchParams({
            token: token,
        }))

        fetchData(token);
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Input
                name='token'
                control={control}
                />
            <button type="submit">Submit</button>
        </form>
    )
}

export default Bearer