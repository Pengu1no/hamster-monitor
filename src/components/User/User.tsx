import {BalanceStore, TokenStore, UserStore} from "../../stores/account.ts";
import styles from './user.module.scss';
import {API} from "../../api/base.ts";
import {UserResponseSchema} from "../../stores/schemas.ts";
import {AxiosResponse} from "axios";
import {useStore} from "@nanostores/react";
import {useEffect, useRef} from "react";
import {Avatars} from "./hamsters.ts";

interface Response {
    telegramUser: UserResponseSchema;
}

const getUserInfo = async () => {
    const token = TokenStore.getToken();

    const res = await API.post<Response, AxiosResponse<Response>>('auth/me-telegram',
        {},
        {headers: {Authorization: `Bearer ${token}`}}
    )

    const {...user} = res.data.telegramUser;

    user.bot = user.isBot ? "YES" : "NO";

    UserStore.setUserStore(user);
}

const User = () => {
    const token = useStore(TokenStore.$tokenStore);
    const user = useStore(UserStore.$userStore);
    const balance = useStore(BalanceStore.$balanceStore);

    const debounceUser = useRef<ReturnType<typeof setTimeout>>();

    useEffect(() => {
        if (!!token && !debounceUser.current) {
            debounceUser.current = setTimeout(() => getUserInfo(), 300);
        }
    })

    if (!user || !balance) {
        return <></>
    }

    return (
        <div className={styles.user}>
            <div className={styles.userCol}>
                {!!user.id && <div><span className={styles.colName}>ID:</span> <span className={styles.colVal}>{user.id}</span></div>}
                {!!user.bot && <div><span className={styles.colName}>Bot:</span> <span className={styles.colVal}>{user.bot}</span></div>}
                {!!user.firstName && <div><span className={styles.colName}>First Name:</span> <span className={styles.colVal}>{user.firstName}</span></div>}
                {!!user.lastName && <div><span className={styles.colName}>Last Name:</span> <span className={styles.colVal}>{user.lastName}</span></div>}
                {!!user.username && <div><span className={styles.colName}>Telegram Username:</span> <span className={styles.colVal}>{user.username}</span></div>}
                {!!user.languageCode && <div><span className={styles.colName}>Language:</span> <span className={styles.colVal}>{user.languageCode}</span></div>}
            </div>
            <div className={styles.userCol}>
                <picture>
                    <source src={Avatars.get(balance.level)?.avif} type="image/avif"/>
                    <source src={Avatars.get(balance.level)?.webp} type="image/webp"/>
                    <img className="img-responsive" src={Avatars.get(balance.level)?.png} alt="Hamster Kombat"/>
                </picture>
            </div>
        </div>
    )
}

export default User
