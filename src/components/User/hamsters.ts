import h1avif from '../../assets/hamsters/1/1.avif';
import h1png from '../../assets/hamsters/1/1.png';
import h1webp from '../../assets/hamsters/1/1.webp';

import h2avif from '../../assets/hamsters/2/2.avif';
import h2png from '../../assets/hamsters/2/2.png';
import h2webp from '../../assets/hamsters/2/2.webp';

import h3avif from '../../assets/hamsters/3/3.avif';
import h3png from '../../assets/hamsters/3/3.png';
import h3webp from '../../assets/hamsters/3/3.webp';

import h4avif from '../../assets/hamsters/4/4.avif';
import h4png from '../../assets/hamsters/4/4.png';
import h4webp from '../../assets/hamsters/4/4.webp';

import h5avif from '../../assets/hamsters/5/5.avif';
import h5png from '../../assets/hamsters/5/5.png';
import h5webp from '../../assets/hamsters/5/5.webp';

import h6avif from '../../assets/hamsters/6/6.avif';
import h6png from '../../assets/hamsters/6/6.png';
import h6webp from '../../assets/hamsters/6/6.webp';

import h7avif from '../../assets/hamsters/7/7.avif';
import h7png from '../../assets/hamsters/7/7.png';
import h7webp from '../../assets/hamsters/7/7.webp';

import h8avif from '../../assets/hamsters/8/8.avif';
import h8png from '../../assets/hamsters/8/8.png';
import h8webp from '../../assets/hamsters/8/8.webp';

import h9avif from '../../assets/hamsters/9/9.avif';
import h9png from '../../assets/hamsters/9/9.png';
import h9webp from '../../assets/hamsters/9/9.webp';

import h10avif from '../../assets/hamsters/10/10.avif';
import h10png from '../../assets/hamsters/10/10.png';
import h10webp from '../../assets/hamsters/10/10.webp';

export interface Avatar {
    avif: string;
    png: string;
    webp: string;
}

export const Avatars: Map<number, Avatar> = new Map([
    [1, {avif: h1avif, png: h1png, webp: h1webp, }],
    [2, {avif: h2avif, png: h2png, webp: h2webp, }],
    [3, {avif: h3avif, png: h3png, webp: h3webp, }],
    [4, {avif: h4avif, png: h4png, webp: h4webp, }],
    [5, {avif: h5avif, png: h5png, webp: h5webp, }],
    [6, {avif: h6avif, png: h6png, webp: h6webp, }],
    [7, {avif: h7avif, png: h7png, webp: h7webp, }],
    [8, {avif: h8avif, png: h8png, webp: h8webp, }],
    [9, {avif: h9avif, png: h9png, webp: h9webp, }],
    [10, {avif: h10avif, png: h10png, webp: h10webp, }],
])