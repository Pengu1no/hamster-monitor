import styles from './navigation.module.scss'
import {AppLink} from "../../ui/AppLink/AppLink.tsx";
import {createSearchParams, useSearchParams} from "react-router-dom";

const NavBar = () => {
    const [query] = useSearchParams();

    const token = query.get('token');

    let search = ''

    if (token) {
        search = `?${createSearchParams({
            token: token
        })}`
    }

    return (
        <div className={styles.tabs}>
            <AppLink to={{
                pathname: "/",
                search: search
            }}>Main</AppLink>
            <AppLink to={{
                pathname: "/cipher",
                search: search
            }}>Daily Cipher</AppLink>
            <AppLink to={{
                pathname: "/user",
                search: search
            }}>User Info</AppLink>
        </div>
    )
}

export default NavBar
