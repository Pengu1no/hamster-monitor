import {TokenStore} from "../../stores/account.ts";
import {API} from "../../api/base.ts";
import {FieldValues, useForm} from "react-hook-form";
import {Input} from "../../ui/Input/Input.tsx";

const enterCipher = async (cipher: string) => {
    const token = TokenStore.getToken();
    await API.post('clicker/claim-daily-cipher',
        {cipher: cipher},
        {headers: {Authorization: `Bearer ${token}`}}
    );
}

const Cipher = () => {
    const {
        handleSubmit,
        control
    } = useForm<FieldValues>({
        values: {
            cipher: '',
        }
    });

    const onSubmit = async (data: FieldValues) => {
        const {
            cipher
        } = data;

        await enterCipher(cipher);
    }



    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <label htmlFor="cipher">Enter Daily Cipher:</label>
            <Input name='cipher' control={control} />
            <button type="submit">Enter</button>
        </form>
    )
}

export default Cipher;