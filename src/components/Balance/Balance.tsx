import styles from './balance.module.scss'
import {BalanceStore, ConfigStore, TokenStore} from "../../stores/account.ts";
import {useStore} from "@nanostores/react";
import {Dispatch, FC, SetStateAction, useEffect, useRef, useState} from "react";
import {addSpaces} from "../../utils";

interface BalanceProps {
    setTarget: Dispatch<SetStateAction<number>>;
}

const selectLevel = (level: number, callback: Dispatch<SetStateAction<number>>) => {
    const balance = BalanceStore.getBalance();
    if (!balance) {
        return
    }
    const config = ConfigStore.getConfig();
    const levelData = config.get(level);
    if (!levelData || !levelData.coins) {
        return;
    }

    if (balance.level > level) {
        return;
    }

    const millisEstimated = ((levelData.coins - balance.balanceCoins) / balance.earnPassivePerSec) * 1000
    const now = new Date();
    callback(levelData.coins);
    balance.rankEstimated = new Date(now.getTime()+millisEstimated).toLocaleString();

    BalanceStore.setBalance(balance);
}

const Balance: FC<BalanceProps> = ({setTarget}) => {
    const token = useStore(TokenStore.$tokenStore);
    const balance = useStore(BalanceStore.$balanceStore);
    const config = useStore(ConfigStore.$configStore);

    const debounce = useRef<ReturnType<typeof setTimeout>>();
    const intervalRef = useRef<ReturnType<typeof setInterval>>();

    useEffect(() => {
        if (!!token && !!balance && !debounce.current) {
            debounce.current = setTimeout(() => selectLevel(balance?.level+1, setTarget), 1000);
        }

        if (balance) {
            intervalRef.current = setInterval(() => {
                BalanceStore.setBalance({...balance, balanceCoins: balance.balanceCoins + balance.earnPassivePerSec})
            }, 1000);
        }

        return () => {
            if (intervalRef.current) {
                clearInterval(intervalRef.current);
            }
        };
    }, [token, balance, config]);

    const [selectedLevel, setSelectedLevel] = useState<string>(!!balance?.level ? (balance.level+1).toString : '1');

    if (!balance) {
        return <></>
    }

    return (
        <div className={styles.balance}>
            <div className={styles.balanceCol}>
                <div>
                    <span className={styles.colName}>Balance:</span>
                    <span className={styles.colVal}>{balance && addSpaces(balance.balanceCoins)}</span>
                </div>
                <div>
                    <span className={styles.colName}>Taps:</span>
                    <span className={styles.colVal}>{balance && addSpaces(balance.availableTaps)}</span>
                </div>
                <div>
                    <span className={styles.colName}>Last Update:</span>
                    <span className={styles.colVal}>{balance && balance.lastSyncUpdate ? new Date(balance.lastSyncUpdate * 1000).toLocaleString() : 'N/A'}</span>
                </div>
                <div>
                    <span className={styles.colName}>Selected Exchange:</span>
                    <span className={styles.colVal}>{balance && balance.exchangeId ? balance.exchangeId : 'N/A'}</span>
                </div>
                <div>
                    <span className={styles.colName}>
                        <select className={styles.input} id="level"
                                value={selectedLevel}
                                onChange={(e) => {
                                    setSelectedLevel(e.target.value);
                                    selectLevel(+e.target.value, setTarget);
                                }}>
                            {
                                [...config.entries()].map(item =>
                                    item[0] > balance.level && <option key={item[0]} value={item[0].toString()}>{item[1].name}</option>
                                )
                            }
                        </select> Estimated:
                    </span>
                    <span className={styles.colVal}>{balance && balance.rankEstimated ? balance.rankEstimated : 'N/A'}</span>
                </div>
            </div>
            <div className={styles.balanceCol}>
                <div>
                    <span className={styles.colName}>Earn / s:</span>
                    <span className={styles.colVal}>{balance && addSpaces(balance.earnPassivePerSec)}</span>
                </div>
                <div>
                    <span className={styles.colName}>Earn / h:</span>
                    <span className={styles.colVal}>{balance && addSpaces(balance.earnPassivePerHour)}</span>
                </div>
                <div>
                    <span className={styles.colName}>Earn / day:</span>
                    <span className={styles.colVal}>{balance && addSpaces(balance.earnPassivePerDay)}</span>
                </div>
                <div>
                    <span className={styles.colName}>Earn / week:</span>
                    <span className={styles.colVal}>{balance && addSpaces(balance.earnPassivePerWeek)}</span>
                </div>
                <div>
                    <span className={styles.colName}>Last Passive Earn:</span>
                    <span className={styles.colVal}>{balance && addSpaces(balance.lastPassiveEarn)}</span>
                </div>
            </div>
        </div>
    )
}

export default Balance