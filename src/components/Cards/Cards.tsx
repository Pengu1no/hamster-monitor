import {BalanceStore, CardsStore, FilteredStore, TokenStore} from "../../stores/account.ts";
import styles from './cards.module.scss'
import {API} from "../../api/base.ts";
import {CardsResponseSchema} from "../../stores/schemas.ts";
import {AxiosResponse} from "axios";
import {useStore} from "@nanostores/react";
import {FC, useEffect, useRef, useState} from "react";
import Card from "./Card/Card.tsx";

interface Response {
    upgradesForBuy: CardsResponseSchema[];
}

const sortCards = (cards: CardsResponseSchema[], filterEfficiency: boolean = false, target: number = 1_000_000_000) => {
    const notExpired = cards.filter(item => !item.isExpired);

    const efficiency = notExpired.map<CardsResponseSchema>(item => ({
        ...item,
        efficiency: item.price > 0 ? (item.profitPerHourDelta / item.price) * 100 : 0
    }));

    let filteredCards = efficiency;
    if (filterEfficiency) {
        const balance = BalanceStore.getBalance();
        if (!!balance) {
            filteredCards = efficiency.filter(card =>
                (target - balance.balanceCoins) * card.profitPerHourDelta > card.price * balance.earnPassivePerHour
            );
        }
    }

    const ordered = filteredCards.sort((a, b) => b.efficiency - a.efficiency);
    FilteredStore.setFilteredCardsStore(ordered);
};

interface CardsProps {
    getBalance: () => {};
    getCards: () => Promise<CardsResponseSchema[]>;
    target: number;
}

const Cards: FC<CardsProps> = ({getBalance, getCards, target}) => {
    const token = useStore(TokenStore.$tokenStore);
    const cards = useStore(FilteredStore.$filteredCards);

    const [checked, setChecked] = useState(false);

    const debounce = useRef<ReturnType<typeof setInterval>>();

    useEffect(() => {
        const fetchCards = async () => {
            if (!!token && !cards.length) {
                const found = await getCards();
                sortCards(found, checked, target);
            }

            if (!!cards.length) {
                clearInterval(debounce.current);
            }
        };

        // if (!!token && !debounce.current && !cards) {
        debounce.current = setInterval(fetchCards, 300);
        // }

        return () => {
            if (debounce.current) {
                clearInterval(debounce.current);
            }
        };
    }, [token, checked, cards]);

    const [selectedPageSize, setSelectedPageSize] = useState<string>('25');

    if (!cards.length) {
        return <></>
    }

    const handleHide = () => {
        setChecked(prevChecked => {
            const newChecked = !prevChecked;
            sortCards(CardsStore.getCardsStore(), newChecked, target);
            return newChecked;
        });
    };

    const upgradeCard = async (upgradeId: string) => {
        const token = TokenStore.getToken();

        const res = await API.post<Response, AxiosResponse<Response>>('clicker/buy-upgrade',
            {timestamp: new Date().getMilliseconds(), upgradeId: upgradeId},
            {headers: {Authorization: `Bearer ${token}`}}
        )

        getBalance();

        const cards = res.data.upgradesForBuy;

        CardsStore.setCardsStore(cards);

        sortCards(cards, checked, target);
    }

    return (
        <div className={styles.cards}>
            <div className={styles.controls}>
                <div>
                    <label htmlFor="count">Number of IDs to Fetch:</label>
                    <select className={styles.input} id="count" value={selectedPageSize} onChange={(e) => setSelectedPageSize(e.target.value)}>
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </div>
                <div>
                    <label htmlFor="hide">Hide inefficient:</label>
                    <input className={styles.input} type="checkbox" checked={checked} onChange={handleHide}/>
                </div>
            </div>
            <table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Section</th>
                    <th>Efficiency %</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {cards.slice(0, +selectedPageSize).map((card) => (
                    <Card key={card.id} card={card} upgradeCard={upgradeCard}/>
                ))}
                </tbody>
            </table>
        </div>
    )
}

export default Cards;
