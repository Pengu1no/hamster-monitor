import {CardsResponseSchema} from "../../../stores/schemas.ts";
import {FC, useEffect, useRef, useState} from "react";
import {addSpaces} from "../../../utils";
import UpgradeButton from "../UpgradeButton/UpgradeButton.tsx";

interface CardProps {
    card: CardsResponseSchema;
    upgradeCard: (id: string) => {};
}

const Card: FC<CardProps> = ({ card, upgradeCard }) => {
    const [cooldown, setCooldown] = useState(card.cooldownSeconds);
    const intervalRef = useRef<ReturnType<typeof setInterval>>();

    useEffect(() => {
        if (cooldown > 0) {
            intervalRef.current = setInterval(() => {
                setCooldown((prevCooldown) => Math.max(prevCooldown - 1, 0));
            }, 1000);
        }

        return () => {
            if (intervalRef.current) {
                clearInterval(intervalRef.current);
            }
        };
    }, [cooldown]);

    useEffect(() => {
        setCooldown(card.cooldownSeconds);
    }, [card.cooldownSeconds]);

    return (
        <tr key={card.id}>
            <td>{card.name}</td>
            <td>{card.section}</td>
            <td>{card.efficiency.toFixed(3)}</td>
            <td>{addSpaces(card.price)}</td>
            <td>
                <UpgradeButton
                    id={card.id}
                    cooldown={cooldown}
                    callback={upgradeCard}
                    disabled={!card.isAvailable || cooldown > 0}
                />
            </td>
        </tr>
    );
};

export default Card;