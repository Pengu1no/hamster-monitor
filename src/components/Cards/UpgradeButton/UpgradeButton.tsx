import {FC} from "react";

interface UpgradeButtonProps {
    id: string;
    callback: (id: string) => {}
    disabled?: boolean;
    cooldown?: number;
}

const UpgradeButton: FC<UpgradeButtonProps> = (props) => {
    const {
        id,
        callback,
        disabled,
        cooldown,
    } = props;

    const upgrade = async () => {
        callback(id);
    }

    const text = !!cooldown ? new Date(cooldown * 1000).toISOString().slice(11, 19) : "Upgrade";

    return (
        <button onClick={upgrade} disabled={disabled}>{text}</button>
    )
}

export default UpgradeButton;