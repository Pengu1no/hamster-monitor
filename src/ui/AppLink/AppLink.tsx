import {FC} from 'react'
import styles from './appLink.module.scss';
import cn from 'classnames';
import {LinkProps, NavLink} from "react-router-dom";

interface AppLinkProps extends LinkProps {
}

export const AppLink: FC<AppLinkProps> = (props) => {
    const {
        children,
        ...rest
    } = props;

    return (
        <NavLink
            className={({ isActive }) =>
                cn(styles.link, {[styles.active]: isActive})
            }
            {...rest}
        >
            {children}
        </NavLink>
    )
}
