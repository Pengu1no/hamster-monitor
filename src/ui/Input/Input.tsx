import {useController, UseControllerProps} from "react-hook-form";
import {FC} from "react";

interface InputProps extends UseControllerProps {
    type?: string;
}

export const Input: FC<InputProps> = (props) => {
    const { type = 'text', ...rest } = props;

    const {field} = useController(rest);

    return (
        <input
            type={type}
            {...field}
            />
    )
}